require('dotenv').config();
const express = require('express');
const app = express();
const { dbConnect } = require('./config/mongo');

const PORT = process.env.PORT || 3000;

app.use(express.json())

app.use('/api', require('./src/routes'))

dbConnect();
app.listen(PORT, () =>{
    console.log('API listening on PORT ', PORT);
})