const paymentIntentModel = require('../models/paymentIntent');
const { getStatus } = require('../controllers/users');
const fetch = require('node-fetch');

const getPaymentsIntent = async (req, res) => {
    try {
        const listAll = await paymentIntentModel.find({});
        res.send({ data:listAll });
    } catch (error) {
        httpError(res, error);
    }
}

const getPaymentIntent = async (req, res) => {
    try {
        const paymentIntent = await paymentIntentModel.find({ _id:req.params.id });
        res.send({ data:paymentIntent });
    } catch (error) {
        httpError(res, error);
    }
}

const createPaymentIntent = async (req, res) => {
    try {
        const { product, price, comerceUrl } = req.body;
        const paymentDetail = await paymentIntentModel.create({ product, price, comerceUrl });
        res.send({ data:paymentDetail });
    } catch (error) {
        httpError(res, error);
    }
}

const payIntention = async (req, res) => {
    try{
        const { userUrl, userId } = req.body;
        let payment = await paymentIntentModel.findOne({ _id : req.params.id });
        const comerceUrl = payment.comerceUrl;
        const enabledUser = await getStatus(userId);
        if( enabledUser ) {
            payment = await paymentIntentModel.findOneAndUpdate({ _id:req.params.id }, {state : 'payment', userUrl },{new: true});
        } else {
            const tries = payment.try;
            payment = await paymentIntentModel.findOneAndUpdate({ _id:req.params.id, userUrl }, {state : 'refused', try: tries+1}, {new: true});
            ckeckPaymentBlock(payment);
        }
        const urls = [comerceUrl, userUrl];
        for( let i = 0; i < urls.length; i++ ) {
            await fetch(urls[i],{
                method: 'POST',
                body : JSON.stringify(payment)
            });
        }
        return res.send({data: payment});
    } catch (error) {
        httpError(res, error);
    }
}

const ckeckPaymentBlock = async (payment) => {
    if( payment.try >= 3){
        await paymentIntentModel.findOneAndUpdate({ _id:payment._id }, { state : 'blocked' });
    }
}

module.exports = { payIntention, getPaymentIntent, createPaymentIntent, getPaymentsIntent };