const { httpError } = require('../helpers/handleError')
const { validationResult } = require('express-validator');
const shopModel = require('../models/shops')
const { generateToken } = require('./auth');
const { checkUser } = require('../helpers/utils')


const getShops = async (res) => {
    try {
        const listAll = await shopModel.find({});
        res.send({ data:listAll });
    } catch (error) {
        httpError(res, error);
    }
}

const getShop = async (req, res) => {
    try {
        const shop = await shopModel.find({ _id:req.params.id });
        res.send({ data:shop });
    } catch (error) {
        httpError(res, error);
    }
}

const createShop = async (req, res) => {
    try {
        const { name, email, cuit } = req.body;
        const password = await generateToken(req);
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            return res.status(400).json({ errors: errors.array() });
        }
        const resDetail = await shopModel.create({ name, email, cuit, password });
        res.send({ data:resDetail });
    } catch (error) {
        httpError(res, error);
    }
}

const updateShop = async (req, res) => {
    if( checkUser(req) ) {
        res.status(400);
        res.send({ error: 'Acceso Denegado' });
    } else {
        try {
            const { name, email, cuit } = req.body;
            const filter = { _id:req.params.id };
            const resDetail = await shopModel.findOneAndUpdate(filter, { name, email, cuit });
            res.send({ data:resDetail });
        } catch (error) {
            httpError(res, error);
        }
    }
}

const deleteShop = async (req, res) => {
    if( checkUser(req) ) {
        res.status(400);
        res.send({ error: 'Acceso Denegado' });
    } else {
        try {
            const filter = { _id:req.params.id };
            const resDetail = await shopModel.deleteOne(filter);
            res.send({ data:resDetail });
        } catch (error) {
            httpError(res, error);
        }
    }
}

module.exports = { getShops, getShop, createShop, updateShop, deleteShop };