const { validationResult } = require('express-validator');
const { httpError } = require('../helpers/handleError');
const userModel = require('../models/users');
const { generateToken } = require('./auth');
const { checkUser } = require('../helpers/utils')

const getUsers = async (req, res) => {
    try {
        const listAll = await userModel.find({});
        res.send({ data:listAll });
    } catch (error) {
        httpError(res, error);
    }
}

const getUser = async (req, res) => {
    try {
        const user = await userModel.find({ _id:req.params.id });
        res.send({ data:user });
    } catch (error) {
        httpError(res, error);
    }
}

const createUser = async (req, res) => {
    const { name, age, email, limit, creditCard, debt } = req.body;
    try {
        const password = await generateToken(req);
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            return res.status(400).json({ errors: errors.array() });
        }
        const resDetail = await userModel.create({ name, age, email, password, limit, creditCard, debt });
        res.send({ data:resDetail });
    } catch (error) {
        httpError(res, error);
    }
}

const updateUser = async (req, res) => {
    if( checkUser(req) ) {
        res.status(400);
        res.send({ error: 'Acceso Denegado' });
    } else {
        try {
            const { name, age, email, limit, creditCard, debt } = req.body;
            const filter = { _id:req.params.id };
            const resDetail = await userModel.findOneAndUpdate(filter, { name, age, email, limit, creditCard, debt });
            res.send({ data:resDetail });
        } catch (error) {
            httpError(res, error);
        }
    }
}

const deleteUser = async (req, res) => {
    if( checkUser(req) ) {
        res.status(400);
        res.send({ error: 'Acceso Denegado' });
    } else {
        try {
            const filter = { _id:req.params.id };
            const resDetail = await userModel.deleteOne(filter);
            res.send({ data:resDetail });
        } catch (error) {
            httpError(res, error);
        }
    }
}

const getStatus = async (id) => {
    const user = await userModel.findOne({ _id:id });
    const { limit, creditCard, debt } = user;
    return ( limit > 0 && creditCard && !debt );
}

module.exports = { getUsers, getUser, createUser, updateUser, deleteUser, getStatus };