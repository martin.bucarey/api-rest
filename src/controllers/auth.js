const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');
const userModel = require('../models/users');

const login = async (req, res) => {
    // TODO: busqueda de email y comapare password para shops o users
    const token = jwt.sign({
        name: req.body.name,
        id: req.body.id,
        password: req.body.password
    }, process.env.TOKEN_SECRET)
    
    res.header('Authorization', token).json({
        error: null,
        data: { token }
    })
}

const verifyToken = (req, res, next) => {
    const token = req.header('Authorization');
    if (!token) return res.status(401).json({ error: 'Acceso denegado' });
    try {
        const verified = jwt.verify(token, process.env.TOKEN_SECRET);
        req.user = verified;
        next();
    } catch (error) {
        res.status(400).json({error: 'token inválido'});
    }
}

const generateToken = async (req) => {
    const salt = await bcrypt.genSalt(10);
    const password = await bcrypt.hash(req.body.password, salt);
    return password;
}

module.exports = { login, verifyToken, generateToken };