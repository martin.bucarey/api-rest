const mongoose = require('mongoose');

const ShopSchema = new mongoose.Schema({
    name: String,
    email: String,
    cuit: Number,
    password: String
})

module.exports = mongoose.model('Shops', ShopSchema)