const mongoose = require('mongoose');

const PaymentIntentSchema = new mongoose.Schema({
    product: String,
    price: Number,
    try: {
        type: Number,
        default: 0
    },
    state: {
        type: String,
        default: 'on-hold'
    },
    comerceUrl: String,
    userUrl: String
})

module.exports = mongoose.model('PaymentIntent', PaymentIntentSchema)