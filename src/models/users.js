const mongoose = require('mongoose');

const UserSchema = new mongoose.Schema({
    name: String,
    email: String,
    age: Number,
    password: String,
    limit: Number,
    creditCard: Boolean,
    debt: Boolean
})

module.exports = mongoose.model('Users', UserSchema)