const express = require('express');
const router = express.Router();
const { getShops, getShop, createShop, updateShop, deleteShop } = require('../controllers/Shops');
const { verifyToken } = require('../controllers/auth');
const { validator } = require('../helpers/validator');

router.get('/:id', verifyToken, getShop)

router.get('/', verifyToken, getShops)

router.post('/', validator(), createShop)

router.patch('/:id', verifyToken, updateShop)

router.delete('/:id', verifyToken, deleteShop)


module.exports = router