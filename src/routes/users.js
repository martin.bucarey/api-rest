const express = require('express');
const router = express.Router();
const { getUsers, getUser, createUser, updateUser, deleteUser } = require('../controllers/users');
const { validator } = require('../helpers/validator');
const { verifyToken } = require('../controllers/auth');

router.get('/:id', verifyToken, getUser)

router.get('/', verifyToken, getUsers)

router.post('/', validator(), createUser)

router.patch('/:id', verifyToken, updateUser)

router.delete('/:id', verifyToken, deleteUser)


module.exports = router