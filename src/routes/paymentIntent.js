const express = require('express');
const router = express.Router();
const { verifyToken } = require('../controllers/auth');
const { getPaymentIntent, createPaymentIntent, payIntention, getPaymentsIntent } = require('../controllers/paymentIntent');

router.get('/:id', verifyToken, getPaymentIntent)

router.get('/', verifyToken, getPaymentsIntent)

router.post('/', verifyToken, createPaymentIntent)

router.post('/payIntention/:id',verifyToken, payIntention);

module.exports = router