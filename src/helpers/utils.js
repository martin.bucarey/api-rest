const checkUser = (req) => {
    const token = req.header('Authorization');
    const { id } =  JSON.parse(Buffer.from(token.split('.')[1], 'base64').toString());
    return id!==req.params.id ;
}

module.exports = { checkUser };