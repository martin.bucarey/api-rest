const { body } = require('express-validator');

const validator = () => {
    return [
        body('email').isEmail(),
        body('password').isLength({ min: 6 })
    ];
}

module.exports = { validator };