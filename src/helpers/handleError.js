const httpError = (res, err) => {
    res.status(500);
    res.send({ error : 'Error'});
}

module.exports = { httpError };