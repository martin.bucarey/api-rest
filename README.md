Diseñar y desarrollar una API REST con las siguientes tecnologías:

- Express (o framework a elección): https://expressjs.com/es/
- MongoDB (o noSQL a elección): https://www.mongodb.com/
- Mongoose (u ODM a elección): https://mongoosejs.com/
- Express Validator (o validador a elección): https://express-validator.github.io/docs/
- Jsonwebtoken (o método de auth a elección): https://www.npmjs.com/package/jsonwebtoken

La solución debe permitir las siguientes acciones:

- CRUD de usuarios
- CRUD de comercios
- Tanto los usuarios como los comercios pueden crear una cuenta, y luego (mediante login) poder acceder a los demás recursos según corresponda
- Solo pueden modificarse o eliminarse a sí mismos (“comercio A” no puede modificar a “comercio B”, etc)
- Los comercios generan intenciones de pago (que especifican producto que se quiere comprar, precio, etc) para que luego cualquier usuario la tomar y pagar haciendo efectiva la compra
- Cuando un usuario paga, el sistema debe validar que tenga límite disponible, que su tarjeta de crédito esté activa y que no posea deuda pendiente.
- Sea cual sea el resultado del pago, se debe enviar una callback a la url que especificaron previamente usuario (en el contrato del pago) y comercio (en el contrato de la intención de pago) informando el estado del mismo https://webhook.site/
- Una intención de pago se puede intentar pagar 3 veces hasta que sea aprobada, luego de eso queda bloqueada
